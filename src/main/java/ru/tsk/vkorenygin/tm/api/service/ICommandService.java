package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
